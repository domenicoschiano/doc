resource "azurerm_resource_group" "rg-nat" {
  name     = var.rg_nat_name
  location = var.rg_nat_location
}

resource "azurerm_public_ip" "NAT-pub-ip" {
  name                = var.nat_gateway_pub_IP_name
  location            = azurerm_resource_group.rg-nat.location
  resource_group_name = azurerm_resource_group.rg-nat.name
  allocation_method   = "Static"
  sku                 = var.sku_pub_ip
}

resource "azurerm_public_ip_prefix" "NAT-pub-prefix" {
  name                = var.nat_gateway_pub_prefix_name
  location            = azurerm_resource_group.rg-nat.location
  resource_group_name = azurerm_resource_group.rg-nat.name
  prefix_length       = 30
}

resource "azurerm_nat_gateway" "NAT-Gateway" {
  name                    = var.nat_Gateway_name
  location                = azurerm_resource_group.rg-nat.location
  resource_group_name     = azurerm_resource_group.rg-nat.name
  public_ip_address_ids   = [azurerm_public_ip.NAT-pub-ip.id]
  public_ip_prefix_ids    = [azurerm_public_ip_prefix.NAT-pub-prefix.id]
  sku_name                = "Standard"
  idle_timeout_in_minutes = 10
}