output "Nat-Public-IP-id" {
  description = "Local peering ID"
  value       = azurerm_public_ip.NAT-pub-ip.id
}

output "Nat-Public-Pub-Prefix-id" {
  description = "Local peering ID"
  value       = azurerm_public_ip_prefix.NAT-pub-prefix.id
}