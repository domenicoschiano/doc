variable "rg_nat_name" {
  description = "Default resource group name that the NAT will be created in"
}

variable "rg_nat_location" {
  description = "Default resource group location that the NAT will be created in"
}

variable "nat_gateway_pub_IP_name" {
  description = "Name of Public IP"
}

variable "sku_pub_ip" {
  description = "Possible value basic or standard"
}

variable "nat_gateway_pub_prefix_name" {
  description = "Name of Public IP Prefix"
}

variable "nat_Gateway_name" {
  description = "Name of the NAT Gateway"
}

