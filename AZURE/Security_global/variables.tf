variable "rg_name" {
    description = "name of resorce group"
    type = string 
}

variable "rg_location" {
    description = "location of resorce group"
    type = string 
}

variable "nsg_name" {
    description = "name of netwrok security group"
    type = string 
}

variable "subnet_id" {
    description = "subnet_id"
    type = string 
}

variable "rdp_direction" {
    description = "direction inbound/outbound"
    type = string
    default = "Inbound" 
}

variable "rdp_access" {
    description = "access allow/deny"
    type = string
    default = "Allow" 
}

variable "rdp_src" {
    description = "source allowed to rdp"
    type = string
}

variable "rdp_dst" {
    description = "dst of rdp"
    type = string
}

variable "sql_direction" {
    description = "direction inbound/outbound"
    type = string
    default = "Inbound" 
}

variable "sql_access" {
    description = "access allow/deny"
    type = string
    default = "Allow" 
}

variable "sql_src" {
    description = "source allowed to sql"
    type = string
}

variable "sql_dst" {
    description = "dst of sql"
    type = string
}

variable "http_direction" {
    description = "direction inbound/outbound"
    type = string
    default = "Inbound" 
}

variable "http_access" {
    description = "access allow/deny"
    type = string
    default = "Allow" 
}

variable "http_src" {
    description = "source allowed to http"
    type = string
}

variable "http_dst" {
    description = "dst of http"
    type = string
}

variable "https_direction" {
    description = "direction inbound/outbound"
    type = string
    default = "Inbound" 
}

variable "https_access" {
    description = "access allow/deny"
    type = string
    default = "Allow" 
}

variable "https_src" {
    description = "source allowed to https"
    type = string
}

variable "https_dst" {
    description = "dst of https"
    type = string
}