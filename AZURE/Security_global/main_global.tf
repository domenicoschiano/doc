resource "azurerm_resource_group" "rg-nsg-global" {
  name     = var.rg_name
  location = var.rg_location
}
 
resource "azurerm_network_security_group" "nsg-global" {
  name                = var.nsg_name
  location            = var.rg_location
  resource_group_name = azurerm_resource_group.rg-nsg-global.name
 
}
 
resource "azurerm_network_security_rule" "testrules" {
  for_each                    = local.nsgrules 
  name                        = each.key
  direction                   = each.value.direction
  access                      = each.value.access
  priority                    = each.value.priority
  protocol                    = each.value.protocol
  source_port_range           = each.value.source_port_range
  destination_port_range      = each.value.destination_port_range
  source_address_prefix       = each.value.source_address_prefix
  destination_address_prefix  = each.value.destination_address_prefix
  resource_group_name         = azurerm_resource_group.rg-nsg-global.name
  network_security_group_name = azurerm_network_security_group.nsg-global.name
}

resource "azurerm_subnet_network_security_group_association" "sg-association-global" {
  subnet_id                 = var.subnet_id
  network_security_group_id = azurerm_network_security_group.nsg-global.id
}