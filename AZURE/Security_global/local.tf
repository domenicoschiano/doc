locals { 
nsgrules = {
   
    rdp = {
      name                       = "rdp"
      priority                   = 100
      direction                  = var.rdp_direction
      access                     = var.rdp_access
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range    = "3389"
      source_address_prefix      = var.rdp_src
      destination_address_prefix = var.rdp_dst
    }
 
    sql = {
      name                       = "sql"
      priority                   = 110
      direction                  = var.sql_direction
      access                     = var.sql_access
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "1433"
      source_address_prefix      = var.sql_src
      destination_address_prefix = var.sql_dst
    }
 
    http = {
      name                       = "http"
      priority                   = 120
      direction                  = var.http_direction
      access                     = var.http_access
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "80"
      source_address_prefix      = var.http_src
      destination_address_prefix = var.http_dst
    }

    https = {
      name                       = "https"
      priority                   = 120
      direction                  = var.https_direction
      access                     = var.https_access
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "443"
      source_address_prefix      = var.https_src
      destination_address_prefix = var.https_dst
    }
  }
 
}