variable "rg_name" {
    description = "name of resorce group"
    type = string 
}

variable "rg_location" {
    description = "location of resorce group"
    type = string 
}

variable "nsg_name" {
    description = "name of netwrok security group"
    type = string 
}

variable "subnet_id" {
    description = "subnet_id"
    type = string 
}