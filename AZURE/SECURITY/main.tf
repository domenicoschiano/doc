resource "azurerm_resource_group" "rg-nsg-termod" {
  name     = var.rg_name
  location = var.rg_location
}

resource "azurerm_network_security_group" "nsg-termod" {
  name                = var.nsg_name
  location            = azurerm_resource_group.rg-nsg-termod.location
  resource_group_name = azurerm_resource_group.rg-nsg-termod.name
}

resource "azurerm_network_security_rule" "sec-rule" {
  name                        = "sec-rule-tag-termod"
  direction                   = "outbound"
  access                      = "Allow"
  priority                    = 100
  source_address_prefix       = "*"
  source_port_range           = "*"
  destination_address_prefix  = "MicrosoftCloudAppSecurity"
  destination_port_range      = "*"
  protocol                    = "*"
  resource_group_name         = azurerm_resource_group.rg-nsg-termod.name
  network_security_group_name = azurerm_network_security_group.nsg-termod.name
}

resource "azurerm_subnet_network_security_group_association" "sg-association-termod" {
  subnet_id                 = var.subnet_id
  network_security_group_id = azurerm_network_security_group.nsg-termod.id
}

