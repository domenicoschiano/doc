resource "azurerm_resource_group" "rg-peering" {
  name     = var.rg_peering_name
  location = var.rg_peering_location
}

resource "azurerm_virtual_network_peering" "VNET1-to-VNET2" {
  name                      = var.peering1_name
  resource_group_name       = azurerm_resource_group.rg-peering.name
  virtual_network_name      = var.vnet1_name
  remote_virtual_network_id = var.vnet2_id
}

resource "azurerm_virtual_network_peering" "VNET2-to-VNET1" {
  name                      = var.peering2_name
  resource_group_name       = azurerm_resource_group.rg-peering.name
  virtual_network_name      = var.vnet2_name
  remote_virtual_network_id = var.vnet1_id
}