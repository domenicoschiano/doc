variable "rg_peering_name" {
  description = "Default resource group name that the peering will be created in."
}

variable "rg_peering_location" {
  description = "The location/region where the peering will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
}

variable "peering1_name" {
  description = "The name of the peer will be created"
}

variable "peering2_name" {
  description = "The name of the VNET will be created"
}

variable "vnet1_name" {
  description = "The name of the VNET created. Check module VNET"
}

variable "vnet2_name" {
  description = "The name of the VNET created. Check module VNET"
}

variable "vnet1_id" {
  description = "The id of the VNET created. Check module VNET"
}

variable "vnet2_id" {
  description = "The id of the VNET created. Check module VNET"
}