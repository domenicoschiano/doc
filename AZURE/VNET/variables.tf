variable "rg_name" {
    description = "name of resorce group"
    type = string 
}

variable "rg_location" {
    description = "location of resorce group"
    type = string 
}

variable "vnet_name" {
    description = "Name of VNET"
    type = string
}

variable "vnet_address_space" {
    description = "CIDR for the Private Subnet"
    type = string
}


variable "subname" {
    description = "name of each subnets"
    type = list
}

variable "subnets_count" {
    description = "Number of subnets per VPC"
    type = number
}

variable "nbit" {
    description = "bits number for network mask"
    type = number
}