output "vnet" {
  value = azurerm_virtual_network.vnet-termod.id
}

output "vnet-name" {
  value = azurerm_virtual_network.vnet-termod.name
}

output "subnets" {
  value = azurerm_subnet.subnet-termod[0].id
}

output "subnet-dmz" {
  value = azurerm_subnet.subnet-termod[1].id
}

output "subnet-ids" {
  value = azurerm_subnet.subnet-termod.*.id
}