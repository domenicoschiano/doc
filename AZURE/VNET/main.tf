resource "azurerm_resource_group" "vnet-rg-termod" {
  name     = var.rg_name
  location = var.rg_location
}

resource "azurerm_virtual_network" "vnet-termod" {
  name                = var.vnet_name
  location            = azurerm_resource_group.vnet-rg-termod.location
  resource_group_name = azurerm_resource_group.vnet-rg-termod.name
  address_space       = [var.vnet_address_space]
}

resource "azurerm_subnet" "subnet-termod" {
  count                = var.subnets_count
  name                 = var.subname[count.index]
  resource_group_name  = azurerm_resource_group.vnet-rg-termod.name
  virtual_network_name = azurerm_virtual_network.vnet-termod.name
  address_prefixes     = [cidrsubnet(var.vnet_address_space,var.nbit,count.index)]
}

