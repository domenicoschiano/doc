resource "aws_security_group" "security_group_termod" {
  name        = var.sg_name
  description = var.sg_description
  vpc_id      = var.vpc_termod

  ingress {  
    description = var_ingress_1
    from_port   = var.ing_1_from_port
    to_port     = var.ing_1_to_port
    protocol    = var.ing_1_protocol
    cidr_blocks = [var.ing_1_src]
    }
ingress {
    description = var_ingress_2
    from_port   = var.ing_2_from_port
    to_port     = var.ing_2_to_port
    protocol    = var.ing_2_protocol
    cidr_blocks = [var.ing_2_src]
    }
egress {  
    description = var_egress_1
    from_port   = var.egress_1_from_port
    to_port     = var.egress_1_to_port
    protocol    = var.egress_1_protocol
    cidr_blocks = [var.egress_1_dst]
    }
egress {
    description = var_egress_2
    from_port   = var.egress_2_from_port
    to_port     = var.egress_2_to_port
    protocol    = var.egress_2_protocol
    cidr_blocks = [var.egress_2_dst]
    }
tags = {
    Name = var.sg_name
  }
}