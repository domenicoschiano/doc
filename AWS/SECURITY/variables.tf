variable "sg_name" {
    description = "Name of Security Group"
    type = string
}

variable "sg_description" {
    description = "Description of Security Group"
    type = string
}


variable "vpc_termod" {
    description = "vpc id"
    type = string
}

variable "ingress_1" {
    description = "Description of rule"
    type = string
}

variable "ing_1_from_port" {
    description = "first port to filter"
    type = number
}

variable "ing_1_to_port" {
    description = "last port to filter"
    type = number
}

variable "ing_1_protocol" {
    description = "protocol to filter"
    type = string

}

variable "ing_1_src" {
    description = "source to filter"
    type = string

}


variable "ingress_2" {
    description = "Description of rule"
    type = string
}

variable "ing_2_from_port" {
    description = "first port to filter"
    type = number
}

variable "ing_2_to_port" {
    description = "last port to filter"
    type = number
}

variable "ing_2_protocol" {
    description = "protocol to filter"
    type = string

}

variable "ing_2_src" {
    description = "source to filter"
    type = string

}

variable "egress_1" {
    description = "Description of rule"
    type = string
}

variable "egress_1_from_port" {
    description = "first port to filter"
    type = number
}

variable "egress_1_to_port" {
    description = "last port to filter"
    type = number
}

variable "egress_1_protocol" {
    description = "protocol to filter"
    type = string

}

variable "egress_1_dst" {
    description = "destination to filter"
    type = string

}

variable "egress_2" {
    description = "Description of rule"
    type = string
}

variable "egress_2_from_port" {
    description = "first port to filter"
    type = number
}

variable "egress_2_to_port" {
    description = "last port to filter"
    type = number
}

variable "egress_2_protocol" {
    description = "protocol to filter"
    type = string

}

variable "egress_2_dst" {
    description = "destination to filter"
    type = string

}

variable "sg_name" {
  description = "Security group name"
  type = string
}

