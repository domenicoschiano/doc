resource "aws_ec2_transit_gateway" "tgw-termod" {
  amazon_side_asn = 64532 

  tags = {
    Name = "tgw-termod"
  }
}

resource "aws_ec2_transit_gateway_vpc_attachment" "attach-termod" {
  subnet_ids         = [var.subnets_dmz]
  transit_gateway_id = aws_ec2_transit_gateway.tgw-termod.id
  vpc_id             = var.vpc_termod

 tags = {
    Name = "attach-termod"
 }
}