resource "aws_nat_gateway" "natgw-termod" {
  allocation_id = aws_eip.public.id
  subnet_id     = var.subnets_termod

  tags = {
    Name = var.natgw_name
  }
}