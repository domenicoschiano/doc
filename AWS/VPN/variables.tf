variable "vpc_termod" {
    description = "vpc id"
    type = string
}

variable "vpn_gw_name" {
    description = "vpn gateway name"
    type = string
}

variable "cust_vpn_gw_name" {
    description = "customer vpn gateway name"
    type = string
}

variable "customer_vpn_ip" {
    description = "private subnet id"
    type = string
}

variable "sum_cidr_blocks" {
    description = "private subnet id"
    type        = string
}

