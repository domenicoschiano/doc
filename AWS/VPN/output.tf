output "vpn-gateway" {
  value = aws_vpn_gateway.vpn-gw-termod.id
}

output "customer-vpn-gateway" {
  value = aws_customer_gateway.customer-gw-termod.id
}


output "vpn-conx" {
  value = aws_vpn_connection.vpn-conx-termod.id
}
