resource "aws_vpn_gateway" "vpn-gw-termod" {
  vpc_id = var.vpc_termod

  tags = {
    Name = var.vpn_gw_name
  }
}

resource "aws_customer_gateway" "customer-gw-termod" {
  bgp_asn    = 65000
  ip_address = var.customer_vpn_ip
  type       = "ipsec.1"

  tags = {
    Name = var.cust_vpn_gw_name
  }
}

resource "aws_vpn_connection" "vpn-conx-termod" {
  vpn_gateway_id      = aws_vpn_gateway.vpn-gw-termod.id
  customer_gateway_id = aws_customer_gateway.customer-gw-termod.id
  type                = "ipsec.1"
  static_routes_only  = true
  tunnel1_ike_versions = ["ikev2"]
  tunnel2_ike_versions = ["ikev2"]
  tunnel1_phase1_dh_group_numbers = [2]
  tunnel2_phase1_dh_group_numbers = [2]
  tunnel1_phase1_encryption_algorithms = ["AES256"]
  tunnel1_phase2_encryption_algorithms = ["AES256"]
  tunnel2_phase1_encryption_algorithms = ["AES256"]
  tunnel2_phase2_encryption_algorithms = ["AES256"]
  tunnel1_phase1_integrity_algorithms  = ["SHA2-256"]
  tunnel2_phase1_integrity_algorithms  = ["SHA2-256"]
}

resource "aws_vpn_connection_route" "vpn_conx_termod" {
  destination_cidr_block = var.sum_cidr_blocks
  vpn_connection_id      = aws_vpn_connection.vpn-conx-termod.id
}