output "internet-gateway" {
  value = aws_internet_gateway.intgw-termod.id
}

output "elastic-ip-id" {
  description = "Contains the EIP allocation ID"
  value       = aws_eip.public.id
}

output "public-ip" {
  value = aws_eip.public.public_ip
}