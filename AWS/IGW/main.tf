resource "aws_internet_gateway" "intgw-termod" {
  vpc_id = var.vpc_termod

  tags = {
    Name = var.igw_name
  }
}

resource "aws_eip" "public" {
  vpc   = true

  tags = {
    Name = var.eip_name
    }
}