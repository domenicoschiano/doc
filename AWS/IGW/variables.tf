variable "vpc_termod" {
    description = "vpc id"
    type = string
}

variable "igw_name" {
    description = "Internet Gateway Name"
    type = string
}

variable "eip_name" {
    description = "Elastic IP name"
    type = string
}


