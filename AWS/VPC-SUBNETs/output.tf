output "vpc-termod" {
  value = aws_vpc.vpc-termod.id
}

output "subnet-public" {
  value = aws_subnet.subnets-termod[0].id
}

output "subnet-private" {
  value = aws_subnet.subnets-termod[1].id
}

output "subnet-ids" {
  value = aws_subnet.subnets-termod.*.id
}