## DEFINE MAIN VPC

resource "aws_vpc" "vpc-termod" {
  cidr_block           = var.vpc_subnet_cidr
  enable_dns_hostnames = true
    tags               = {
    Name               = var.vpc_name
  }
}

## DEFINE SUBNET
resource "aws_subnet" "subnets-termod" {
  count      = var.subnets_count
  vpc_id     = aws_vpc.vpc-termod.id
  cidr_block = cidrsubnet(var.vpc_subnet_cidr,var.nbit,count.index)
    tags     = {
    Name     = var.subname[count.index]
  }
}


