variable "vpc_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    type = string
}

variable "vpc_name" {
    description = "Name of VPC"
    type = string
}

variable "subnets_count" {
    description = "Number of subnets per VPC"
    type = number
}

variable "nbit" {
    description = "bits number for network mask"
    type = number
}

variable "subname" {
    description = "name of each subnets"
    type = list
}




