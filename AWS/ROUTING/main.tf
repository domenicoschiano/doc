resource "aws_route_table" "public-termod" {
  vpc_id = var.vpc_termod

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.igw_gateway
  }

  tags = {
    Name = var.pub_rtable_name
  }
}

resource "aws_route_table_association" "rt-association-termod" {
  subnet_id      = var.subnets_public
  route_table_id = aws_route_table.public-termod.id
}

resource "aws_route_table" "internal-termod" {
  vpc_id = var.vpc_termod

  route {
    cidr_block         = var.cidr_blocks
    gateway_id         = var.vpn_gateway
  }

   route {
    cidr_block         = var.second_cidr_blocks
    gateway_id         = var.vpn_gateway
  }

  tags = {
    Name = var.priv_rtable_name
  }
}

resource "aws_route_table_association" "rt-association-internal-termod" {
  subnet_id      = var.subnets_private
  route_table_id = aws_route_table.internal-termod.id
}