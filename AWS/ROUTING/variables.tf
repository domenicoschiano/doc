variable "vpc_termod" {
    description = "vpc id"
    type = string
}

variable "subnets_public" {
    description = "subnets id"
    type = string
}

variable "igw_gateway" {
    description = "internet gateway id"
    type = string
}

variable "pub_rtable_name" {
    description = "name of public route table"
    type = string
}

variable "subnets_private" {
    description = "private subnet id"
    type = string
}

variable "cidr_blocks" {
    description = "private subnet"
    type = string
}

variable "second_cidr_blocks" {
    description = "second private subnet"
    type = string
}

variable "vpn_gateway" {
    description = "VPN Gateway id"
    type = string
}

variable "priv_rtable_name" {
    description = "name of private route table"
    type = string
}
