output "public-route-table" {
  value = aws_route_table.public-termod.id
}

output "private-route-table" {
  value = aws_route_table.internal-termod.id
}

